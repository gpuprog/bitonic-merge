#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

__global__ void bitonicSplit(float *sequence, int totalLength, int halfLength) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < totalLength) {
    int indexInSubseq = i % halfLength;
    int whatSubseq = i / halfLength;
    int index = halfLength * 2 * whatSubseq + indexInSubseq;
    float min = sequence[index] <= sequence[index + halfLength] ? sequence[index] : sequence[index + halfLength];
    float max = sequence[index] >= sequence[index + halfLength] ? sequence[index] : sequence[index + halfLength];
    sequence[index] = min;
    sequence[index + halfLength] = max;
  }
}

void bitonicMerge(float *sequence, int len) {
  int currentLength = len;
  while (currentLength > 1) {
    int halfLength = currentLength / 2;
    dim3 dimBlock(1024, 1, 1);
    dim3 dimGrid((int) ceil((double) len / 2 / 1024), 1, 1);
    bitonicSplit<<<dimGrid, dimBlock>>>(sequence, len, halfLength);
    currentLength = halfLength;
  }
}

int main(int argc, char **argv) {
  int size = argc > 1 ? atoi(argv[1]) : 16;
  int sizeInBytes = size * sizeof(float);
  float *sequence, *result;
  cudaMallocHost((float **) &sequence, sizeInBytes);
  cudaMallocHost((float **) &result, sizeInBytes);

  srand(time(NULL));
  init(sequence, size);
  //printArray(sequence, size);

  float *devSequence;
  cudaMalloc((float **) &devSequence, sizeInBytes);

  cudaMemcpy(devSequence, sequence, sizeInBytes, cudaMemcpyHostToDevice);

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);

  bitonicMerge(devSequence, size);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  printf("sorted elements: %d\n", size);
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  printf("elapsed time: %.5f ms\n", milliseconds);

  cudaMemcpy(result, devSequence, sizeInBytes, cudaMemcpyDeviceToHost);

  //printArray(result, size);
  if (checkSorted(result, size)) {
    printf("sequence is sorted\n");
  } else {
    printf("sequence not sorted!\n");
  }
}
