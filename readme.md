## Benchmarks

| Algorithm                |           2^10 els. |      2^20 els. |
| ------------------------ | ------------------: | -------------: |
| quicksort                |            0.141 ms |     112.151 ms |
| cuda bitonic (recursive) |          4.42784 ms |  3348.79907 ms |
| cuda bitonic (iterative) |          0.07789 ms |     2.08813 ms |
