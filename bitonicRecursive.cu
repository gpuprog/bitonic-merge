#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

__global__ void bitonicSplit(float *sequence, int halfLength) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < halfLength) {
    float min = sequence[i] <= sequence[halfLength + i] ? sequence[i] : sequence[halfLength + i];
    float max = sequence[i] >= sequence[halfLength + i] ? sequence[i] : sequence[halfLength + i];
    sequence[i] = min;
    sequence[halfLength + i] = max;
  }
}

void bitonicMerge(float *sequence, int len) {
  if (len == 1) {
    return;
  }
  int halfLength = len / 2;
  dim3 dimBlock(1024, 1, 1);
  dim3 dimGrid((int) ceil((double) halfLength / 1024), 1, 1);
  bitonicSplit<<<dimGrid, dimBlock>>>(sequence, halfLength);
  bitonicMerge(sequence, halfLength);
  bitonicMerge(&sequence[halfLength], halfLength);
}

int main(int argc, char **argv) {
  int size = argc > 1 ? atoi(argv[1]) : 16;
  int sizeInBytes = size * sizeof(float);
  float *sequence, *result;
  cudaMallocHost((float **) &sequence, sizeInBytes);
  cudaMallocHost((float **) &result, sizeInBytes);

  srand(time(NULL));
  init(sequence, size);
  //printArray(sequence, size);

  float *devSequence;
  cudaMalloc((float **) &devSequence, sizeInBytes);

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaMemcpy(devSequence, sequence, sizeInBytes, cudaMemcpyHostToDevice);

  cudaEventRecord(start);
  bitonicMerge(devSequence, size);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  cudaMemcpy(result, devSequence, sizeInBytes, cudaMemcpyDeviceToHost);

  printf("sorted elements: %d\n", size);
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  printf("elapsed time: %.5f ms\n", milliseconds);

  //printArray(result, size);
  if (checkSorted(result, size)) {
    printf("sequence is sorted\n");
  } else {
    printf("sequence not sorted!\n");
  }
}
