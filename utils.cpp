#include <stdio.h>
#include <stdlib.h>
#include <time.h>

bool checkSorted(float *a, int len) {
  if (len == 0) {
    return true;
  }
  float last = a[0];
  for (int i = 1; i < len; i++) {
    if (last > a[i]) {
      return false;
    }
    last = a[i];
  }
  return true;
}

void printArray(float *a, int len) {
  for (int i = 0; i < len; i++) {
    printf("%.2f ", a[i]);
  }
  printf("\n");
}

void initMonotonicAsc(float *a, int len, float startFrom, float arriveTo, int maxIncrement) {
  float val = startFrom;
  for (int i = 0; i < len; i++) {
    val += rand() % maxIncrement + 1;
    a[i] = val;
  }
  float factor = (arriveTo - startFrom) / (val - startFrom);
  for (int i = 0; i < len; i++) {
    a[i] = (a[i] - startFrom) * factor + startFrom;
  }
}

void initMonotonicDsc(float *a, int len, float startFrom, float arriveTo, int maxIncrement) {
  float val = startFrom;
  for (int i = 0; i < len; i++) {
    val -= rand() % maxIncrement + 1;
    a[i] = val;
  }
  float factor = (startFrom - arriveTo) / (startFrom - val);
  for (int i = 0; i < len; i++) {
    a[i] = (a[i] - startFrom) * factor + startFrom;
  }
}

void init(float *a, int len) {
  int maxIndex = rand() % len;
  int minIndex = rand() % len;
  if (maxIndex > minIndex) {
    int tmp;
    tmp = maxIndex;
    maxIndex = minIndex;
    minIndex = tmp;
  }
  initMonotonicAsc(a, maxIndex, 0, 100, 100);
  initMonotonicDsc(&a[maxIndex], minIndex - maxIndex, 100, -100, 100);
  initMonotonicAsc(&a[minIndex], len - minIndex, -100, 0, 100);
}

/*
int main() {
  srand(time(NULL));
  float array[20];
  init(array, 20);
  printArray(array, 20);
}
*/
