#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "utils.h"

int compareFloat(const void *a, const void *b) {
  float diff = *((float *) a) - *((float *) b);
  return diff > 0 ? 1 : diff < 0 ? -1 : 0;
}

int main(int argc, char **argv) {
  int size = argc > 1 ? atoi(argv[1]) : 16;
  int sizeInBytes = size * sizeof(float);
  float *sequence = (float *) malloc(sizeInBytes);

  srand(time(NULL));
  init(sequence, size);

  struct timeval start, end;

  gettimeofday(&start, NULL);
  qsort(sequence, size, sizeof(float), compareFloat);
  gettimeofday(&end, NULL);

  printf("sorted elements: %d\n", size);
  float milliseconds = (end.tv_sec - start.tv_sec) * 1000.0 + (end.tv_usec - start.tv_usec) / 1000.0;
  printf("elapsed time: %.5f ms\n", milliseconds);
  if (checkSorted(sequence, size)) {
    printf("sequence is sorted\n");
  } else {
    printf("sequence not sorted!\n");
  }
}
